var files =
[
    [ "alloc_funcs.c", "alloc__funcs_8c_source.html", null ],
    [ "alloc_funcs.h", "alloc__funcs_8h.html", "alloc__funcs_8h" ],
    [ "dcovsrt.c", "dcovsrt_8c_source.html", null ],
    [ "dfit.c", "dfit_8c_source.html", null ],
    [ "dfour1.c", "dfour1_8c_source.html", null ],
    [ "dfourn.c", "dfourn_8c_source.html", null ],
    [ "dgammln.c", "dgammln_8c_source.html", null ],
    [ "dgammq.c", "dgammq_8c_source.html", null ],
    [ "dgasdev.c", "dgasdev_8c_source.html", null ],
    [ "dgaussj.c", "dgaussj_8c_source.html", null ],
    [ "dgcf.c", "dgcf_8c_source.html", null ],
    [ "dgser.c", "dgser_8c_source.html", null ],
    [ "dlfit.c", "dlfit_8c_source.html", null ],
    [ "dnrutil.c", "dnrutil_8c_source.html", null ],
    [ "dnrutil.h", "dnrutil_8h.html", null ],
    [ "doxy-comment.c", "doxy-comment_8c.html", "doxy-comment_8c" ],
    [ "dran1.c", "dran1_8c_source.html", null ],
    [ "fractalClouds.c", "fractal_clouds_8c.html", "fractal_clouds_8c" ]
];