var savgol__usage_8c =
[
    [ "ii1", "savgol__usage_8c.html#a6e93487e1f6d2943af8b5e3ad7e9d509", null ],
    [ "nCoeff", "savgol__usage_8c.html#aad73ba6f90e1f61608bacf249ecb729e", null ],
    [ "nRCoeff", "savgol__usage_8c.html#abca022940392a7a7f669e89fc26f6fbb", null ],
    [ "order_der", "savgol__usage_8c.html#a37da9bcc7528fdec15943c5d6ab91135", null ],
    [ "order_poly", "savgol__usage_8c.html#ab8a2041c06c11de4e8f888a0a7d12711", null ],
    [ "smoothLogPowSpecLogNorm", "savgol__usage_8c.html#abfb802fe91473fed4fb0ec9b64313e87", null ]
];